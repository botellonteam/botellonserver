-- You can use this file to load seed data into the database using SQL statements
--load data local infile 'C://Users/josep/project2/omplirBD/venedor.txt' into table venedor LINES TERMINATED BY '\r\n';
--load data local infile 'C://Users/josep/project2/omplirBD/iva.txt' into table tipus_iva LINES TERMINATED BY '\r\n';
--load data local infile 'C://Users/josep/project2/omplirBD/families.txt' into table families LINES TERMINATED BY '\r\n';
--load data local infile 'C://Users/josep/project2/omplirBD/productes.txt' into table productes;
--load data local infile 'C://Users/josep/project2/omplirBD/member.txt' into table member ;
--delete from productes where codi_producte=0;
load data local infile 'http://botellonapp-pdsbotellon.rhcloud.com/omplirBD/venedor.txt' into table VENEDOR LINES TERMINATED BY '\n';
load data local infile 'http://botellonapp-pdsbotellon.rhcloud.com/omplirBD/iva.txt' into table TIPUS_IVA LINES TERMINATED BY '\n';
load data local infile 'http://botellonapp-pdsbotellon.rhcloud.com/omplirBD/families.txt' into table FAMILIES LINES TERMINATED BY '\n';
load data local infile 'http://botellonapp-pdsbotellon.rhcloud.com/omplirBD/productes.txt' into table PRODUCTES;
load data local infile 'http://botellonapp-pdsbotellon.rhcloud.com/omplirBD/member.txt' into table Member ;
delete from PRODUCTES where codi_producte=0;
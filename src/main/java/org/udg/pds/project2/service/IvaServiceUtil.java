package org.udg.pds.project2.service;

/**
 * Created with IntelliJ IDEA.
 * User: josep
 * Date: 21/04/13
 * Time: 12:32
 * To change this template use File | Settings | File Templates.
 */
import org.udg.pds.project2.model.Iva;

import javax.ejb.LocalBean;
import javax.ejb.Stateful;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;

@Stateful
@LocalBean
public class IvaServiceUtil
{
    @Inject
    private EntityManager em;

    public List<Iva> getIvas()
    {

        List<Iva> list= em.createQuery("from TIPUS_IVA ").getResultList();
        return list;

    }
    public float findIva(int cod)
    {
        return em.find(Iva.class,cod).getValor();
    }

    public void addIva(int tipus,float valor) throws Exception
    {
        Iva iva=new Iva();
        iva.setIdtipusIva(tipus);
        iva.setValor(valor);

        em.persist(iva);
    }
     
}


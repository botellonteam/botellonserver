package org.udg.pds.project2.service;

/**
 * Created with IntelliJ IDEA.
 * User: josep
 * Date: 28/03/13
 * Time: 0:00
 * To change this template use File | Settings | File Templates.
 */

import org.udg.pds.project2.model.Venda;

import javax.ejb.Remote;
import java.util.List;
@Remote
public interface VendaRemoteService
{
    public List<Venda> getVendes();
    public void addVenda(String codisProductes,String quantitats, String nomClient) throws Exception;
}

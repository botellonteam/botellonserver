package org.udg.pds.project2.service;

/**
 * Created with IntelliJ IDEA.
 * User: josep
 * Date: 1/04/13
 * Time: 19:46
 * To change this template use File | Settings | File Templates.
 */
import org.udg.pds.project2.model.Venedor;

import javax.ejb.LocalBean;
import javax.ejb.Stateful;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;

@Stateful
@LocalBean
public class VenedorServiceUtil
{
    @Inject
    private EntityManager em;

    public List<Venedor> getVenedors()
    {
        return em.createQuery("select v from VENEDOR v").getResultList();
    }

    public void addVenedor(String usuariV, String pass, String nom,String primerCog,String segonCog,int admin,int consul,int actuali,int desferVen) throws Exception
    {
        Venedor venedor=new Venedor();

        venedor.setUsuariVenedor(usuariV);
        venedor.setPassword(pass);
        venedor.setNom(nom);
        venedor.setPrimerCognom(primerCog);
        venedor.setSegonCognom(segonCog);
        venedor.setAdministrar(admin);
        venedor.setConsultes(consul);
        venedor.setActualizar(actuali);
        venedor.setDesterVenda(desferVen);

        em.persist(venedor);
    }
    public String findNomComplertById(long usuari)
    {
        return em.find(Venedor.class,usuari).getNom()+" "+em.find(Venedor.class,usuari).getPrimerCognom()+
               " "+ em.find(Venedor.class,usuari).getSegonCognom();
    }
}


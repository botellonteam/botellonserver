package org.udg.pds.project2.service;

/**
 * Created with IntelliJ IDEA.
 * User: josep
 * Date: 28/03/13
 * Time: 0:06
 * To change this template use File | Settings | File Templates.
 */

import org.udg.pds.project2.model.Iva;
import org.udg.pds.project2.model.LiniaVenda;
import org.udg.pds.project2.model.Producte;
import org.udg.pds.project2.model.Venda;

import javax.ejb.LocalBean;
import javax.ejb.Stateful;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.Date;
import java.util.List;

@Stateful
@LocalBean

public class VendaServiceUtil
 //implements VendaRemoteService, VendaLocalService
{
    @Inject
    private EntityManager em;

    public List<Venda> getVendes()
    {
        return em.createQuery("select v from VENDES v").getResultList();
    }

    public String addVenda(String codisP, String quants, String nomClient,String cognomClient,String adrecaClient) throws Exception
    {
        Venda venda=new Venda();
        //Venedor venedor=em.find(Venedor.class,ven);

        //passem de String a arrays de Longs
        String[] items=codisP.split(",");
        Long[] codis = new Long[items.length];

        for (int i = 0; i < items.length; i++) {
            try {
                codis[i] = Long.parseLong(items[i]);
            } catch (NumberFormatException nfe) {};
        }

        //passem de String a arrays de Integers
         items=quants.split(",");
        int[] qts = new int[items.length];

        for (int i = 0; i < items.length; i++) {
            try {
                qts[i] = Integer.parseInt(items[i]);
            } catch (NumberFormatException nfe) {};
        }

        Date dataH=new Date();
        //consultar preuProducte i iva de cada codisProductes[i]
        //calcular el subtotal de cada linia
        //calcular el total
        float  total=0;

        String faltaStock ="";
        String stocksSotaMinims="";
        for(int i=0;i<codis.length;i++)
        {
            //inserim al taula LINIES_VENDA
            LiniaVenda liniaVenda=new LiniaVenda();
            Producte producte=em.find(Producte.class,codis[i]);
            float preuP=producte.getPreuProducte();
            float ivaP=em.find(Iva.class, producte.getIvaProducte()).getValor();
            //actualizar stock
            if(qts[i]<=producte.getQuantitatStock())  //si hi ha stock realizem la venda
            {
                liniaVenda.setProductesCodiProducte(producte.getCodiProducte());
                liniaVenda.setQuantitat(qts[i]);
                liniaVenda.setVendesNumTiquet(venda.getNumTiquet());
                float subtotal=preuP*liniaVenda.getQuantitat()*(1+ivaP/100);

                //truncar a 2 decimals
                subtotal= subtotal*100;
                Float f = new Float(subtotal);
                subtotal = f.intValue();
                subtotal = subtotal/100;
                liniaVenda.setPreu(subtotal);

                liniaVenda.setVenda(venda);
                liniaVenda.setProducte(producte);
                producte.setQuantitatStock(producte.getQuantitatStock()-qts[i]);

                em.persist(liniaVenda);

                total=total+subtotal;
                if(producte.getQuantitatStock()<=producte.getMinimStock())
                    stocksSotaMinims=stocksSotaMinims+producte.getNomProducte() + " queden  " + producte.getQuantitatStock()+" unitats\n";
            }
            else
            {
                faltaStock=faltaStock+producte.getNomProducte()+" no es pot servir per falta de stock\n";
            }
        }
        if(total>0)  //venda feta
        {
            venda.setDataHora(dataH);

            //truncar a 2 decimals
            total= total*100;
            Float f = new Float(total);
            total = f.intValue();
            total = total/100;

            //persistencia
            venda.setTotal(total);
            //venda.setVenedorCodiVenedor(1);
            venda.setNomClient(nomClient);
            venda.setCognomClient(cognomClient);
            venda.setAdrecaClient(adrecaClient);
            //venda.setVenedor(em.find(Venedor.class, 1));

            em.persist(venda);
            return Long.valueOf(venda.getNumTiquet()).toString();
            /*
            if(!faltaStock.equals("") && !stocksSotaMinims.equals(""))

                return "venda creada parcialment \n"+faltaStock+"\ncal repostar stock\n"+stocksSotaMinims;
                return Long.valueOf(venda.getNumTiquet()).toString();
            else if(!faltaStock.equals(""))
                return "venda creada parcialment \n"+faltaStock;
            else if (!stocksSotaMinims.equals(""))
                //return "venda creada, cal repostar stock \n"+stocksSotaMinims;
            else
                return "venda creada correctament";
               */
        }
        else
            return "venda no realitzada ";

    }
    public  void desferVenda(long numTiq)
    {
        //actualizem el stock
        Query selCodi=em.createQuery("select lv.productesCodiProducte from LINIES_VENDA lv where lv.vendesNumTiquet=1");
        List<Long> codisProductes= selCodi.getResultList();

        Query selQuantitat=em.createQuery("select lv.quantitat from LINIES_VENDA lv where lv.vendesNumTiquet=1");
        List<Integer> quantitats = selQuantitat.getResultList();

        //convertir la List a array
        Integer[] arrayQuan = quantitats.toArray(new Integer[quantitats.size()]);

        Integer i=0;
       for(Long codi:codisProductes)
       {
           Producte producte=em.find(Producte.class,codi);
           producte.setQuantitatStock(producte.getQuantitatStock()+arrayQuan[i]);
           i++;
       }
         //eliminar les linies de venda
        Query q=em.createQuery("delete from LINIES_VENDA lv where lv.vendesNumTiquet=:nt").setParameter("nt",numTiq);
        q.executeUpdate();

        //eliminar la venda
        Query query=em.createQuery("delete from VENDES v where v.numTiquet=:nt").setParameter("nt",numTiq);
        query.executeUpdate();

    }

    public float findTotalById(Long id)
    {
        return em.find(Venda.class,id).getTotal();
    }
}

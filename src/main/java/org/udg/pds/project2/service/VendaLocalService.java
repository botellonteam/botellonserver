package org.udg.pds.project2.service;

/**
 * Created with IntelliJ IDEA.
 * User: josep
 * Date: 27/03/13
 * Time: 23:53
 * To change this template use File | Settings | File Templates.
 */

import org.udg.pds.project2.model.Venda;
import javax.ejb.Local;
import java.util.List;

@Local
public interface VendaLocalService
{
    public List<Venda> getVendes();
    public void  addVenda(String codisProductes, String quantitats, String nomClient) throws Exception;
}

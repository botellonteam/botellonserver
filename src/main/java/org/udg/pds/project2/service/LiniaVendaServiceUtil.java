package org.udg.pds.project2.service;

/**
 * Created with IntelliJ IDEA.
 * User: josep
 * Date: 1/04/13
 * Time: 20:55
 * To change this template use File | Settings | File Templates.
 */

import org.udg.pds.project2.model.LiniaVenda;

import javax.ejb.LocalBean;
import javax.ejb.Stateful;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;

@Stateful
@LocalBean
public class LiniaVendaServiceUtil
{
    @Inject
    private EntityManager em;

    public List<LiniaVenda> getLiniesVenda()
    {
        return em.createQuery("select v from LINIES_VENDA v").getResultList();
    }



    public float findSubTotalById(Long id)
    {
        return em.find(LiniaVenda.class,id).getPreu();
    }
}


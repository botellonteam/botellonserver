package org.udg.pds.project2.service;

/**
 * Created with IntelliJ IDEA.
 * User: josep
 * Date: 28/03/13
 * Time: 21:30
 * To change this template use File | Settings | File Templates.
 */

import org.udg.pds.project2.model.Familia;
import org.udg.pds.project2.model.Producte;

import javax.ejb.LocalBean;
import javax.ejb.Stateful;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

@Stateful
@LocalBean
public class FamiliaServiceUtil
{
    @Inject
    private EntityManager em;

    public List<Familia> getFamilies()
    {

        List<Familia> list= em.createQuery("select f from FAMILIES f").getResultList();
        return list;

    }
    public String findFamiliaCat(long id)
    {
        return em.find(Familia.class,id).getNomFamiliaCat();
    }

    public String findFamiliaEn(long id)
    {
        return em.find(Familia.class,id).getNomFamiliaEn();
    }

    public String findFamiliaEs(long id)
    {
        return em.find(Familia.class,id).getNomFamiliaEs();
    }

      public void addFamilia(String nomFamil) throws Exception
      {
            Familia familia=new Familia();

            familia.setNomFamiliaCat(nomFamil);

            em.persist(familia);
       }
      public List<Producte> mostrarProductesDeFamilia(Long cod)
      {
          Query q=em.createQuery("select p from PRODUCTES p where p.familiesCodiFamilia=:cod");
          q.setParameter("cod",cod);
          List<Producte> list= q.getResultList();

          return list;
      }
}

package org.udg.pds.project2.service;

import org.udg.pds.project2.model.Familia;
import org.udg.pds.project2.model.Producte;

import javax.ejb.LocalBean;
import javax.ejb.Stateful;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;

@Stateful
@LocalBean
/**
 * Created with IntelliJ IDEA.
 * User: josep
 * Date: 1/04/13
 * Time: 12:37
 * To change this template use File | Settings | File Templates.
 */
public class ProducteServiceUtil
{
    @Inject
    private EntityManager em;

    public List<Producte> getProductes()
    {
        List<Producte> list= em.createQuery("select p from PRODUCTES p ").getResultList();
        return list;
    }
    public String findProducte(long id)
    {
        return em.find(Producte.class,id).getNomProducte();
    }

    public String findImageProducte(long id)
    {
        return em.find(Producte.class,id).getImatge();
    }

    public void addProducte(Long cod,String nomP,float preuP,int quantitatS,int minS,long familiesCodiF,int tipusIva,String img) throws Exception
    {
        Producte producte=new Producte();

        Familia familia=em.find(Familia.class,familiesCodiF);
        producte.setCodiProducte(cod);
        producte.setNomProducte( nomP );
        producte.setPreuProducte(preuP);
        producte.setQuantitatStock(quantitatS);
        producte.setMinimStock(minS);
        producte.setFamilia(familia);
        producte.setIva(tipusIva);
        producte.setImatge(img);


        em.persist(producte);
    }  
}

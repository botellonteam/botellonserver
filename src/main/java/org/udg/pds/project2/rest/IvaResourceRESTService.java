package org.udg.pds.project2.rest;

import org.udg.pds.project2.model.Iva;
import org.udg.pds.project2.service.IvaServiceUtil;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.*;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: josep
 * Date: 21/04/13
 * Time: 12:54
 * To change this template use File | Settings | File Templates.
 */
@Path("/iva")
@RequestScoped
public class IvaResourceRESTService
{
    @EJB
    IvaServiceUtil ivaServiceUtil;

    @GET
    @Produces("text/xml")
    public List<Iva> listAllMembers() {
        final List<Iva> results = ivaServiceUtil.getIvas();
        return results;
    }

    @POST
    @Path("/add")
    @Produces("text/text")
    public String addIva(

            @FormParam("idtipusIva") int idtipusIva,
            @FormParam("valor") float valor )
    {

        try
        {
            ivaServiceUtil.addIva(idtipusIva,valor);
        }
        catch (Exception ex)
        {
            return "Error al crear el Iva: "+ex.getMessage();
        }

        return "Iva creada.";
    }

    @GET
    @Path("/{id}")
    @Produces("text/text")
    public String findIva(
            @PathParam("id") int idIva)
    {
         return Float.toString(ivaServiceUtil.findIva(idIva));

    }
}


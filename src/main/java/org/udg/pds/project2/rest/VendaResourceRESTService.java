package org.udg.pds.project2.rest;

import org.udg.pds.project2.model.Venda;
import org.udg.pds.project2.service.VendaServiceUtil;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.*;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: josep
 * Date: 1/04/13
 * Time: 18:42
 * To change this template use File | Settings | File Templates.
 */
@Path("/vendes")
@RequestScoped
public class VendaResourceRESTService
{
    @EJB

    VendaServiceUtil vendaServiceUtil;

    @GET
    @Produces("text/xml")
    public List<Venda> listAllVendes() {
        final List<Venda> results = vendaServiceUtil.getVendes();
        return results;
    }

    @POST
    @Path("/add")
    @Produces("text/text")
    public String addVenda(
            @FormParam("codisProductes") String  codisP,
            @FormParam("quantitats") String quants,
            @FormParam("nomClient") String nomCli,
            @FormParam("cognomClient") String cognomCli,
            @FormParam("adrecaClient") String adrecaCli
    )
    {
        try
        {
            String str=vendaServiceUtil.addVenda(codisP,quants,nomCli,cognomCli,adrecaCli);
            return str;
        }
        catch (Exception ex)
        {
            return( "Error al crear la Venda ");
        }



    }

    @POST
    @Path("/id")
    @Produces("text/text")
    public float findTotalById(
            @FormParam("numTiquet") long numT)
    {
        float total=0;
        try
        {
            total= vendaServiceUtil.findTotalById(numT);
        }
        catch (Exception ex)
        {
            total= 0;
            System.out.println( "Error en la cerca de la venda: "+ex.getMessage());
        }
        return total;
    }
    @GET
    @Path("/desferVenda/{id}")
    @Produces("text/text")
    public String desferVendaById(
            @PathParam("id") long numT)
    {

        try
        {
             vendaServiceUtil.desferVenda(numT);
            return "venda desfeta";
        }
        catch (Exception ex)
        {

           return ( "Error en desfer la venda: "+ex.getMessage());
        }

    }
}

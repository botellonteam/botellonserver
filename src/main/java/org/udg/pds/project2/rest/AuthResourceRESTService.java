package org.udg.pds.project2.rest;

import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Path;
import javax.ws.rs.POST;
import javax.ws.rs.GET;
import javax.ws.rs.FormParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Path("/auth")
@RequestScoped
public class AuthResourceRESTService {

	@POST
	@Produces("text/xml")
	public String authPOST(
			@Context HttpServletRequest req,
			@FormParam("username") String username,
			@FormParam("password") String password) {
		
	    HttpSession session = req.getSession();
        if (session.getAttribute("nameid") != null)
	    	return "<?xml version=\"1.0\"?><msg>Authenticated!</msg>";
	    else
	    {
	    	session.setAttribute("nameid" , "yo");
	    	return "<?xml version=\"1.0\"?><msg>Not authenticated!</msg>";
	    }
	}
	
	@GET
	@Produces("text/xml")
	public String authGET(
			@Context HttpServletRequest req,
			@QueryParam("username") String username,
			@QueryParam("password") String password) {
		
	    HttpSession session = req.getSession();
	    if (session.getAttribute("nameid") != null)
	    	return "<?xml version=\"1.0\"?><msg>Authenticated!</msg>";
	    else
	    {
	    	session.setAttribute("nameid" , "yo");
	    	return "<?xml version=\"1.0\"?><msg>Not authenticated!</msg>";
	    }
	}
}

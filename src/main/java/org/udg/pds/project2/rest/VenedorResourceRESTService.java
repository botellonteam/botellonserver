package org.udg.pds.project2.rest;

import org.udg.pds.project2.model.Venedor;
import org.udg.pds.project2.service.VenedorServiceUtil;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.*;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: josep
 * Date: 1/04/13
 * Time: 20:08
 * To change this template use File | Settings | File Templates.
 */
@Path("/venedors")
@RequestScoped
public class VenedorResourceRESTService
{
    @EJB

    VenedorServiceUtil venedorServiceUtil;

    @GET
    @Produces("text/xml")
    public List<Venedor> listAllMembers() {
        final List<Venedor> results = venedorServiceUtil.getVenedors();
        return results;
    }

    @POST
    @Path("/add")
    @Produces("text/text")
    public String addVenedor(
            @FormParam("usuariVenedor") String usuari,
            @FormParam("password") String pass,
            @FormParam("nom") String nom,
            @FormParam("primerCognom") String  primerCog,
            @FormParam("segonCognom") String segonCog,
            @FormParam("administrar") int admin,
            @FormParam("consultes") int consul,
            @FormParam("actualizar") int actuali,
            @FormParam("desferVenda") int desferVen
            )
    {

        try
        {
            venedorServiceUtil.addVenedor(usuari,pass,nom,primerCog,segonCog,admin,consul,actuali,desferVen);
        }
        catch (Exception ex)
        {
            return "Error al crear el Venedor: "+ex.getMessage();
        }

        return "Venedor creat.";
    }
    @GET
    @Path("/{id}")
    @Produces("text/text")
    public String findNomComplertById(
            @PathParam("id") long usu)
    {
        try
        {
            return venedorServiceUtil.findNomComplertById(usu);
        }
        catch (Exception ex)
        {
            return "Error en la cerca del Venedor: "+ex.getMessage();
        }
    }
}


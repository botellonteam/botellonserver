package org.udg.pds.project2.rest;

import org.udg.pds.project2.model.LiniaVenda;
import org.udg.pds.project2.service.LiniaVendaServiceUtil;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.*;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: josep
 * Date: 1/04/13
 * Time: 21:15
 * To change this template use File | Settings | File Templates.
 */
@Path("/liniesVenda")
@RequestScoped
public class LiniaVendaResourceRESTService
{
    @EJB

    LiniaVendaServiceUtil liniaVendaServiceUtil;

    @GET
    @Produces("text/xml")
    public List<LiniaVenda> listAllLiniesVenda() {
        final List<LiniaVenda> results = liniaVendaServiceUtil.getLiniesVenda();
        return results;
    }

    /*
     @POST
    @Path("/add")
    @Produces("text/text")
   public String addLiniaVenda(
            @FormParam("codisProductes") String  codisP,
            @FormParam("quantitats") String quants,
            @FormParam("nomClient") String nomCli
           )
    {
        //tranformar els strings a arrays de codiProductes i quantitats enters
        try
        {
            total=liniaVendaServiceUtil.addLiniaVenda(codisP,quants,nomCli);
        }
        catch (Exception ex)
        {
            return "Error al crear la Venda: "+ex.getMessage();
        }

        return "Venda creada.";

    }
     */
    @POST
    @Path("/{id}")
    @Produces("text/text")
    public float findSubTotalById(
            @FormParam("id") Long numLin)
    {
        float subtotal=0;
        try
        {
            subtotal= liniaVendaServiceUtil.findSubTotalById(numLin);
        }
        catch (Exception ex)
        {
            subtotal= 0;
            System.out.println( "Error en la cerca de la LiniaVenda: "+ex.getMessage());
        }
        return subtotal;
    }
}

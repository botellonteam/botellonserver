package org.udg.pds.project2.rest;

import org.udg.pds.project2.model.Producte;
import org.udg.pds.project2.service.ProducteServiceUtil;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.*;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: josep
 * Date: 1/04/13
 * Time: 12:50
 * To change this template use File | Settings | File Templates.
 */
@Path("/productes")
@RequestScoped
public class ProducteResourceRESTService 
{
    @EJB

    ProducteServiceUtil producteServiceUtil;

    @GET
    @Produces("text/xml")
    public List<Producte> listAllMembers() {
        final List<Producte> results = producteServiceUtil.getProductes();
        return results;
    }

    @POST
    @Path("/add")
    @Produces("text/text")
    public String addProducte(
            @FormParam("codiProducte") Long cod,
            @FormParam("nomProducte") String nomP,
            @FormParam("preuProducte") float preuP,
            @FormParam("quantitatStock") int  quantS,
            @FormParam("minimStock") int minS,
            @FormParam("familiesCodiFamilia") Long familiesCF,
            @FormParam("iva") int tipusIva,
            @FormParam("imatge") String img )
    {

        try
        {
            producteServiceUtil.addProducte(cod,nomP,preuP,quantS,minS,familiesCF,tipusIva,img);
        }
        catch (Exception ex)
        {
            return "Error al crear el Producte: "+ex.getMessage();
        }

        return "Producte creat.";
    }
    @GET
    @Path("/{id}")
    @Produces("text/text")
    public String findProducte(
            @PathParam("id") long codiProducte)
    {
        try
        {
            return producteServiceUtil.findProducte(codiProducte);
        }
        catch (Exception ex)
        {
            return "Error en la cerca del Producte: "+ex.getMessage();
        }
    }

    @GET
    @Path("/image/{id}")
    @Produces("text/text")
    public String findFamilia(
            @PathParam("id") long codiProducte)
    {
        try
        {
            return producteServiceUtil.findImageProducte(
                    codiProducte);
        }
        catch (Exception ex)
        {
            return "Error al cercar la imatge: "+ex.getMessage();
        }
    }
}

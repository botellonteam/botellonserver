package org.udg.pds.project2.rest;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import org.udg.pds.project2.model.Familia;
import org.udg.pds.project2.model.Producte;
import org.udg.pds.project2.service.FamiliaServiceUtil;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.*;
import java.io.StringWriter;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: josep
 * Date: 28/03/13
 * Time: 0:33
 * To change this template use File | Settings | File Templates.
 */


@Path("/families")
@RequestScoped
public class FamiliaResourceRESTService
{
    @EJB
    FamiliaServiceUtil familiaServiceUtil;

    @GET
    @Produces("text/xml")
    public List<Familia> listAllMembers() {
        final List<Familia> results = familiaServiceUtil.getFamilies();
        return results;
    }

    @POST
    @Path("/add")
    @Produces("text/text")
    public String addFamilia(

            @FormParam("nomFamilia") String nomFamilia)
    {

        try
        {
            familiaServiceUtil.addFamilia(nomFamilia);
        }
        catch (Exception ex)
        {
            return "Error al crear la Familia: "+ex.getMessage();
        }

        return "Familia creada.";
    }
    @GET
    @Path("/{id}")
    @Produces("text/text")
    public String findFamilia(
            @PathParam("id") long codiFamilia)
    {
        try
        {
            return familiaServiceUtil.findFamiliaCat(
                    codiFamilia);
        }
        catch (Exception ex)
        {
            return "Error al buscar la Familia: "+ex.getMessage();
        }
    }

    @GET
    @Path("/{id}/productes")
    @Produces("text/xml")
    public String listProductsOfFamilie(
            @PathParam("id") long codiFamilia)
    {
        List<Producte> results = familiaServiceUtil.mostrarProductesDeFamilia(codiFamilia);

        //crear xml a mida
        Element root = new Element("collection");
        Document doc = new Document(root);
        doc.setRootElement(root);
        for (Producte p: results) {
            Element xp = new Element("producte");
            xp.setText(p.getNomProducte());
            root.addContent(xp);
        }

        XMLOutputter xmlOutput = new XMLOutputter();

        // display nice nice
        xmlOutput.setFormat(Format.getPrettyFormat());
        StringWriter sw = new StringWriter();
        try {
            xmlOutput.output(doc, sw);
        }
        catch (Exception ex) {
            return "error al generar xml"+ex.getMessage();
        }
        return sw.toString();
    }

}

package org.udg.pds.project2.model;

/**
 * Created with IntelliJ IDEA.
 * User: josep
 * Date: 27/03/13
 * Time: 11:24
 * To change this template use File | Settings | File Templates.
 */

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

@Entity (name = "VENDES")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Venda implements Serializable
{
    @Id
    @Column(name="NUM_TIQUET",nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long numTiquet  ;

    @Column(name = "NOM_CLIENT",nullable = true)
    private String nomClient;

    public String getCognomClient() {
        return cognomClient;
    }

    public void setCognomClient(String cognomClient) {
        this.cognomClient = cognomClient;
    }

    public String getAdrecaClient() {
        return adrecaClient;
    }

    public void setAdrecaClient(String adrecaClient) {
        this.adrecaClient = adrecaClient;
    }

    @Column(name = "COGNOM_CLIENT",nullable = true)
    private String cognomClient;

    @Column(name = "ADRECA_CLIENT",nullable = true)
    private String adrecaClient;

    @Column(name = "DATA_HORA",nullable = false)
    private Date dataHora;

    @Column(name="TOTAL",nullable = false)
    private float total;



    @XmlTransient
    @ManyToOne(optional = true,fetch=FetchType.LAZY)
    @JoinColumn(name = "VENEDOR_CODI_VENEDOR", referencedColumnName = "CODI_VENEDOR")
    private Venedor venedor;

    @XmlTransient
    @OneToMany(mappedBy = "venda",targetEntity = LiniaVenda.class)
    Collection<LiniaVenda> liniesVenda;

    public Collection<LiniaVenda> getLiniesVenda() {
        return liniesVenda;
    }

    public void setLiniesVenda(Collection<LiniaVenda> liniesVenda) {
        this.liniesVenda = liniesVenda;
    }


    public Venedor getVenedor() {
        return venedor;
    }

    public void setVenedor(Venedor venedor) {
        this.venedor = venedor;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public Date getDataHora() {
        return dataHora;
    }

    public void setDataHora(Date dataHora) {
        this.dataHora = dataHora;
    }

    public long getNumTiquet() {
        return numTiquet;
    }

    public void setNumTiquet(long numTiquet) {
        this.numTiquet = numTiquet;
    }

    public String getNomClient() {
        return nomClient;
    }

    public void setNomClient(String nomClient) {
        this.nomClient = nomClient;
    }
}

package org.udg.pds.project2.model;

/**
 * Created with IntelliJ IDEA.
 * User: josep
 * Date: 27/03/13
 * Time: 21:56
 * To change this template use File | Settings | File Templates.
 */

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;

@Entity (name = "LINIES_VENDA")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class LiniaVenda implements Serializable
{
    @Id
    @Column(name="NUM_LINIA",nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long numLinia;

    @Column(name = "VENDES_NUM_TIQUET",insertable = false,updatable = false)
    private long vendesNumTiquet;

    @Column(name="PRODUCTES_CODI_PRODUCTE",insertable = false,updatable = false)
    private Long productesCodiProducte;

    @Column(name = "QUANTITAT",nullable = true)
    private int quantitat;

    @Column(name = "PREU",nullable = true)
    private float preu;

    @XmlTransient
    @ManyToOne(optional = true)
    @JoinColumn(name = "PRODUCTES_CODI_PRODUCTE",referencedColumnName = "CODI_PRODUCTE")
    private Producte producte;

    @XmlTransient
    @ManyToOne(optional = true,fetch=FetchType.EAGER)
    @JoinColumn(name = "VENDES_NUM_TIQUET",referencedColumnName = "NUM_TIQUET")
    private Venda venda;

    public Long getNumLinia() {
        return numLinia;
    }

    public void setNumLinia(Long numLinia) {
        this.numLinia = numLinia;
    }

    public long getVendesNumTiquet() {
        return vendesNumTiquet;
    }

    public void setVendesNumTiquet(long vendesNumTiquet) {
        this.vendesNumTiquet = vendesNumTiquet;
    }

    public long getProductesCodiProducte() {
        return productesCodiProducte;
    }

    public void setProductesCodiProducte(Long productesCodiProducte) {
        this.productesCodiProducte = productesCodiProducte;
    }

    public int getQuantitat() {
        return quantitat;
    }

    public void setQuantitat(int quantitat) {
        this.quantitat = quantitat;
    }

    public float getPreu() {
        return preu;
    }

    public void setPreu(float preu) {
        this.preu = preu;
    }

    public Producte getProducte() {
        return producte;
    }

    public void setProducte(Producte producte) {
        this.producte = producte;
    }

    public Venda getVenda() {
        return venda;
    }

    public void setVenda(Venda venda) {
        this.venda = venda;
    }

}

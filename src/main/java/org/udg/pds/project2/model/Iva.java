package org.udg.pds.project2.model;

/**
 * Created with IntelliJ IDEA.
 * User: josep
 * Date: 21/04/13
 * Time: 12:22
 * To change this template use File | Settings | File Templates.
 */

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.util.Collection;

@Entity (name = "TIPUS_IVA")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)

public class Iva implements Serializable {
    @Id
    @Column(name="IDTIPUS_IVA")
    private int idtipusIva;

    @Column(name = "VALOR")
    private float valor;

    public int getIdtipusIva() {
        return idtipusIva;
    }

    public void setIdtipusIva(int idtipusIva) {
        this.idtipusIva = idtipusIva;
    }

    public float getValor() {
        return valor;
    }

    public void setValor(float valor) {
        this.valor = valor;
    }

    @XmlTransient
    @OneToMany(mappedBy = "objIva",targetEntity = Producte.class)
    private Collection<Producte> productes;

    public Collection<Producte> getProductes() {
        return productes;
    }

    public void setProductes(Collection<Producte> productes) {
        this.productes = productes;
    }


}


package org.udg.pds.project2.model;

/**
 * Created with IntelliJ IDEA.
 * User: josep
 * Date: 20/03/13
 * Time: 19:37
 * To change this template use File | Settings | File Templates.
 */

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.util.Collection;

@Entity (name = "FAMILIES")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
//@Table(uniqueConstraints = @UniqueConstraint(columnNames = "NOM_FAMILIA"))
public class Familia implements Serializable {
    @Id
    @Column(name="CODI_FAMILIA")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long codiFamilia;

    @Column(name = "NOM_FAMILIA_CAT")
    private String nomFamiliaCat;

    @Column(name = "NOM_FAMILIA_EN")
    private String nomFamiliaEn;

    @Column(name = "NOM_FAMILIA_ES")
    private String nomFamiliaEs;


    @XmlTransient
    @OneToMany(mappedBy = "familia",targetEntity = Producte.class)
    private Collection<Producte> productes;


    public String getNomFamiliaEn() {
        return nomFamiliaEn;
    }

    public void setNomFamiliaEn(String nomFamiliaEn) {
        this.nomFamiliaEn = nomFamiliaEn;
    }

    public String getNomFamiliaCat() {
        return nomFamiliaCat;
    }

    public void setNomFamiliaCat(String nomFamiliaCat) {
        this.nomFamiliaCat = nomFamiliaCat;
    }

    public String getNomFamiliaEs() {
        return nomFamiliaEs;
    }

    public void setNomFamiliaEs(String nomFamiliaEs) {
        this.nomFamiliaEs = nomFamiliaEs;
    }


    public Collection<Producte> getProductes() {
        return productes;
    }

    public void setProductes(Collection<Producte> productes) {
        this.productes = productes;
    }

    public Long getCodiFamilia() {
        return codiFamilia;
    }

    public void setCodiFamilia(Long codiFamilia) {
        this.codiFamilia = codiFamilia;
    }
}


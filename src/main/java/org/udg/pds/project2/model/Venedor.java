package org.udg.pds.project2.model;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.util.Collection;

/**
 * Created with IntelliJ IDEA.
 * User: josep
 * Date: 27/03/13
 * Time: 9:18
 * To change this template use File | Settings | File Templates.
 */

@Entity (name = "VENEDOR")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
//@Table(uniqueConstraints = @UniqueConstraint(columnNames = "PASSWORD"))
@Table(uniqueConstraints = @UniqueConstraint(columnNames = "USUARI_VENEDOR"))
public class Venedor implements Serializable
{
    @Id
    @Column(name="CODI_VENEDOR")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long codiVenedor;

    public long getCodiVenedor() {
        return codiVenedor;
    }

    public void setCodiVenedor(long codiVenedor) {
        this.codiVenedor = codiVenedor;
    }

    @Column(name = "USUARI_VENEDOR",nullable = false)
    private  String usuariVenedor;

    @Column(name = "PASSWORD",nullable = false)
    private String password;

    @XmlTransient
    @OneToMany(mappedBy = "venedor",targetEntity = Venda.class)
    private Collection<Venda> vendes;

    @Column(name = "NOM",nullable = false)
    private String nom;

    @Column(name = "PRIMER_COGNOM",nullable =false )
    private String primerCognom;

    @Column(name = "SEGON_COGNOM",nullable = true)
    private String segonCognom;

    @Column(name = "ADMINISTRAR",nullable = false)
    private int administrar;

    @Column(name = "CONSULTES",nullable = false)
    private int consultes;

    @Column(name = "ACTUALIZAR",nullable = false)
    private int actualizar;

    @Column(name = "DESFER_VENDA",nullable = false)
    private int desterVenda;

    public String getUsuariVenedor() {
        return usuariVenedor;
    }

    public void setUsuariVenedor(String usuariVenedor) {
        this.usuariVenedor = usuariVenedor;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Collection<Venda> getVendes() {
        return vendes;
    }

    public void setVendes(Collection<Venda> vendes) {
        this.vendes = vendes;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrimerCognom() {
        return primerCognom;
    }

    public void setPrimerCognom(String primerCognom) {
        this.primerCognom = primerCognom;
    }

    public String getSegonCognom() {
        return segonCognom;
    }

    public void setSegonCognom(String segonCognom) {
        this.segonCognom = segonCognom;
    }

    public int getAdministrar() {
        return administrar;
    }

    public void setAdministrar(int administrar) {
        this.administrar = administrar;
    }

    public int getConsultes() {
        return consultes;
    }

    public void setConsultes(int consultes) {
        this.consultes = consultes;
    }

    public int getActualizar() {
        return actualizar;
    }

    public void setActualizar(int actualizar) {
        this.actualizar = actualizar;
    }

    public int getDesterVenda() {
        return desterVenda;
    }

    public void setDesterVenda(int desterVenda) {
        this.desterVenda = desterVenda;
    }
}

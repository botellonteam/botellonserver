package org.udg.pds.project2.model;

/**
 * Created with IntelliJ IDEA.
 * User: josep
 * Date: 19/03/13
 * Time: 18:11
 * To change this template use File | Settings | File Templates.
 */

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.util.Collection;

@Entity (name = "PRODUCTES")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@Table(uniqueConstraints = @UniqueConstraint(columnNames = "NOM_PRODUCTE"))
public class Producte implements Serializable
{

    @Id
    @Column(name="CODI_PRODUCTE")

    private Long codiProducte;

    @Column(name = "NOM_PRODUCTE",nullable = false)
    private String nomProducte;

    public String getDescripcio() {
        return descripcio;
    }

    public void setDescripcio(String descripcio) {
        this.descripcio = descripcio;
    }

    public Collection<LiniaVenda> getLiniesVenda() {
        return liniesVenda;
    }

    public void setLiniesVenda(Collection<LiniaVenda> liniesVenda) {
        this.liniesVenda = liniesVenda;
    }


    @Column(name="PREU_PRODUCTE",nullable = false)
    private float preuProducte;

    @Column(name = "QUANTITAT_STOCK",nullable = true)
    private int quantitatStock;

    @Column(name = "MINIM_STOCK",nullable = true)
    private int minimStock;


    @Column(name = "FAMILIES_CODI_FAMILIA",insertable = false,updatable = false)
    private Long familiesCodiFamilia;

    public Producte(){}

    public int getIvaProducte() {
        return ivaProducte;
    }

    public void setIvaProducte(int ivaProducte) {
        this.ivaProducte = ivaProducte;
    }

    @XmlTransient
    @ManyToOne(optional = true)
    @JoinColumn(name = "FAMILIES_CODI_FAMILIA",nullable = false,referencedColumnName = "CODI_FAMILIA")
    private Familia familia;

    @Column(name = "IVA_PRODUCTE",insertable = false,updatable = false)
    private int ivaProducte;

    @XmlTransient
    @ManyToOne(optional = true)
    @JoinColumn(name = "IVA_PRODUCTE",referencedColumnName = "IDTIPUS_IVA")
    private Iva objIva;

    @Column(name = "IMATGE",nullable = true)
    private String imatge;

    @Column(name = "DESCRIPCIO",nullable = true)
    private String descripcio;

    @XmlTransient
    @OneToMany(mappedBy = "producte",targetEntity = LiniaVenda.class)
    Collection<LiniaVenda> liniesVenda;


    public Long getCodiProducte() {
        return codiProducte;
    }

    public void setCodiProducte(Long codiProducte) {
        this.codiProducte = codiProducte;
    }

    public String getNomProducte() {
        return nomProducte;
    }

    public void setNomProducte(String nomProducte) {
        this.nomProducte = nomProducte;
    }

    public float getPreuProducte() {
        return preuProducte;
    }

    public void setPreuProducte(float preuProducte) {
        this.preuProducte = preuProducte;
    }

    public int getQuantitatStock() {
        return quantitatStock;
    }

    public void setQuantitatStock(int quantitatStock) {
        this.quantitatStock = quantitatStock;
    }

    public int getMinimStock() {
        return minimStock;
    }

    public void setMinimStock(int minimStock) {
        this.minimStock = minimStock;
    }

    public Long getFamiliesCodiFamilia() {
        return familiesCodiFamilia;
    }

    public void setFamiliesCodiFamilia(Long familiesCodiFamilia) {
        this.familiesCodiFamilia = familiesCodiFamilia;
    }

    public Familia getFamilia() {
        return familia;
    }

    public void setFamilia(Familia familia) {
        this.familia = familia;
    }

    public int getIva() {
        return ivaProducte;
    }

    public void setIva(int iva) {
        this.ivaProducte = iva;
    }

    public String getImatge() {
        return imatge;
    }

    public void setImatge(String imatge) {
        this.imatge = imatge;
    }

    public Iva getObjIva() {
        return objIva;
    }

    public void setObjIva(Iva objIva) {
        this.objIva = objIva;
    }
}

